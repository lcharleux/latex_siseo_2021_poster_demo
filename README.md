# LaTeX SISEO 2021 posterdemo

A collaborative poster demo




=======
## Goal

* Create a poster with BAposter (LaTeX)
* Use git collaboratively


Hello! (T)
=======
## Useful commands

* Get repository: `git status`
* Add a file for the next commit: `git add xxx` where `xxx`  is the file name.
* Make a commit: `git commit -m "Comment"`
* Pull modifications: `git pull remote branch` where remote is the url or alias (origin for example) of the remote and `branch` is the name of the target branch.
* Push modifications: `git push remote branch`.

